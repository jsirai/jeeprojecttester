package pages;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class AddPage extends Pages {

    public String url;

    public AddPage(String pageUrl, WebDriver driver) {
        url = pageUrl;
        this.driver = driver;
    }

    public AddPage goTo() {
        goTo(url);
        return new AddPage(url, driver);
    }

    public Menu menu() {
        return new Menu(driver);
    }

    public SearchPage insert(String code) {
        return insert(new CustomerBuilder().withCode(code));
    }

    public SearchPage insert(CustomerBuilder builder) {

        element("firstNameBox").sendKeys(builder.firstName);
        element("surnameBox").sendKeys(builder.surname);
        element("codeBox").sendKeys(builder.code);

        if (isElementPresent("customerTypeSelect")) {
            Select select = new Select(element("customerTypeSelect"));
            select.selectByValue(builder.type);
        }

        element("addButton").click();
        return new SearchPage(url, driver);
    }

    public AddPage addPhoneRow() {
        element("addPhoneButton").click();
        return this;
    }

    public AddPage addPhoneWithValue(String value) {
        element("addPhoneButton").click();

        String elementId = MessageFormat.format("phones{0}.value",
                getChannelCount() - 1);

        element(elementId).sendKeys(value);

        return this;
    }

    public int getChannelCount() {
        return findElementsMatching("phones{0}.id").size();
    }

    public AddPage addFixedPhone(String value) {
        return addPhone("phoneType.fixed", value);
    }

    public AddPage addMobilePhone(String value) {
        return addPhone("phoneType.mobile", value);
    }

    private AddPage addPhone(String type, String value) {
        element("addPhoneButton").click();

        String valueElementId = MessageFormat.format("phones{0}.value",
                getChannelCount() - 1);
        String typeElementId = MessageFormat.format("phones{0}.type",
                getChannelCount() - 1);

        Select select = new Select(element(typeElementId));
        select.selectByValue(type);

        element(valueElementId).sendKeys(value);

        return this;
    }

    public AddPage deleteFirstPhone() {
        element("phones0.deletePressed").click();
        return this;
    }

    public String getFirstPhoneValue() {
        return element("phones0.value").getAttribute("value");
    }

    public List<String> getSelectOptions(String selectId) {
        Select select = new Select(element(selectId));
        List<String> retval = new ArrayList<String>();
        for (WebElement webElement : select.getOptions()) {
            retval.add(webElement.getAttribute("value"));
        }

        return retval;
    }

    public static class CustomerBuilder {
        String firstName = "testFirstName";
        String surname = "testSurname";
        String code = randomString();
        String type = "customerType.private";
        public CustomerBuilder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }
        public CustomerBuilder withSurname(String surname) {
            this.surname = surname;
            return this;
        }
        public CustomerBuilder withCode(String code) {
            this.code = code;
            return this;
        }
        public CustomerBuilder withType(String type) {
            this.type = type;
            return this;
        }
    }

}
