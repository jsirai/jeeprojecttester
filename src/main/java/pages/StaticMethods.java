package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class StaticMethods {

    public static SearchPage goToSearchPage() {
        return Pages.createSearchPage(Pages.getNewDriver()).goTo();
    }

    public static AddPage goToAddPage() {
        return Pages.createAddPage(Pages.getNewDriver()).goTo();
    }

    public static String randomString() {
        return Pages.randomString();
    }

    public static AddPage.CustomerBuilder customer() {
        return new AddPage.CustomerBuilder();
    }

    public static SearchPage logInWith(String username, String password) {
        WebDriver driver = Pages.getNewDriver();
        SearchPage searchPage = Pages.createSearchPage(driver).goTo();
        logInWith(driver, username, password);
        return searchPage;
    }

    public static void logInWith(WebDriver driver, String username, String password) {
        element(driver, "usernameBox").clear();
        element(driver, "usernameBox").sendKeys(username);
        element(driver, "passwordBox").clear();
        element(driver, "passwordBox").sendKeys(password);
        element(driver, "logInButton").click();
    }

    public static WebElement element(WebDriver driver, String id) {
        return driver.findElement(By.id(id));
    }

}
