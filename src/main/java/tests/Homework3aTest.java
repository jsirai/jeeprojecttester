package tests;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static pages.StaticMethods.*;

import org.junit.Before;
import org.junit.Test;

import pages.*;

public class Homework3aTest {

    @Before
    public void setUp() {
        Pages.setBaseUrl("http://mkalmo-hw3.appspot.com/");
    }

    @Test
    public void searchPageExists_10() {
        assertTrue(goToSearchPage().isSearchPage());
    }

    @Test
    public void addPageExists_5() {
        assertTrue(goToAddPage().isAddPage());
    }

    @Test
    public void searchPageHasValidMenu_2() {
        assertValid(goToSearchPage().menu());
    }

    @Test
    public void addPageHasValidMenu_2() {
        assertValid(goToAddPage().menu());
    }

    @Test
    public void sampleDataIsDisplayed_3() {
        // clear data link should not do anything at the moment (href="#")
        SearchPage searchPage = goToSearchPage().menu().clearData();

        // insert sample data link should not do anything at the moment (href="#")
        searchPage.menu().insertSampleData();

        assertThat(searchPage.getCodes(), contains("123", "456", "789"));
    }

    @Test
    public void redirectsAfterInsert_1() {
        SearchPage searchPage = goToAddPage().insert(randomString());

        assertThat(searchPage.getPagePartFromUrl(), is(Pages.getSearchPageUrl()));
    }

    public void assertValid(Menu menu) {
        assertThat(menu.getIds(), contains(
                "menu_Search", "menu_Add", "menu_ClearData", "menu_InsertData"));
    }
}
