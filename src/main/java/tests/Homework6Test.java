package tests;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static pages.StaticMethods.*;

import org.junit.*;

import pages.*;

public class Homework6Test {

    @Before
    public void setUp() {
        Pages.setBaseUrl("http://mkalmo-hw6.appspot.com/");
        Pages.setSearchPageUrl("search");
        Pages.setAddPageUrl("addForm");
    }

    @Test
    public void phonesCanBeAddedToForm_5() {
        AddPage addPage = goToAddPage().addPhoneRow()
                                       .addPhoneRow();

        assertThat(addPage.getChannelCount(), is(2));
    }

    @Test
    public void phonesCanBeDeletedFromForm_5() {
        AddPage addPage = goToAddPage().addPhoneRow()
                                       .addPhoneWithValue("1");

        addPage.deleteFirstPhone();

        assertThat(addPage.getFirstPhoneValue(), is("1"));
    }

    @Test
    public void insertedPhoneValuesAreDisplayed_5() {
        String testCode = randomString();

        AddPage addPage = goToAddPage()
                .addPhoneWithValue("1")
                .addPhoneWithValue("2");
        SearchPage searchPage = addPage.insert(testCode);

        ViewPage viewPage = searchPage.view(testCode);

        assertThat(viewPage.getPhoneValues(),
                contains("1", "2"));
    }

    @Test
    public void addedPhonesHaveCorrectPhonesTypes_1() {
        AddPage addPage = goToAddPage().addPhoneRow();

        assertThat(addPage.getSelectOptions("phones0.type"),
                contains("phoneType.fixed", "phoneType.mobile"));
    }

    @Test
    public void insertedPhoneTypesAreDisplayed_1() {
        String testCode = randomString();

        AddPage addPage = goToAddPage().addFixedPhone("1")
                                       .addMobilePhone("1");

        SearchPage searchPage = addPage.insert(testCode);

        ViewPage viewPage = searchPage.view(testCode);

        assertThat(viewPage.getPhoneTypes(),
                contains("phoneType.fixed", "phoneType.mobile"));
    }
}
