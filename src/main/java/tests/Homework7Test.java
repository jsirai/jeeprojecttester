package tests;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;
import static pages.StaticMethods.*;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.junit.*;

import pages.*;

public class Homework7Test {

    private static String USER = "user";
    private static String ADMIN = "admin";

    @Before
    public void setUp() {
        // Pages.setBaseUrl("http://mkalmo-hw7.appspot.com/");
        Pages.setBaseUrl("http://localhost:8080/jeeHomework7/");
        Pages.setSearchPageUrl("search");
        Pages.setAddPageUrl("addForm");
    }

    @Test
    public void loginPageIsPresented_3() {
        assertTrue(goToSearchPage().isLoginPage());
    }

    @Test
    public void logOutTakesToLogInPage_1() {
        Pages page = logInToSearchPage().menu().logOut();
        assertTrue(page.isLoginPage());
    }

    @Test
    public void languageCanBeChanged_4() {
        String s1 = logInToSearchPage().menu().selectFirstLanguage().getSource();
        String s2 = logInToSearchPage().menu().selectSecondLanguage().getSource();

        int difference = StringUtils.getLevenshteinDistance(
                    Jsoup.parse(s1).text(), Jsoup.parse(s2).text());

        assertThat(difference, greaterThan(10));
    }

    @Test
    public void customerNameAndCodeAreValidated_4() {
        String invalidName = "";
        String invalidCode = "%!&";

        logInToSearchPage().menu().clearData();

        Pages page = logInToAddPage().insert(
                customer().withFirstName(invalidName));

        assertTrue(page.hasMessageBlock());

        page = logInToAddPage().insert(
                customer().withCode(invalidCode));

        assertTrue(page.hasMessageBlock());

        page = logInToAddPage().insert(customer());
        assertTrue(page.isSearchPage());
    }

    @Test
    public void phoneValuesAreValidated_1() {
        logInToSearchPage().menu().clearData();
        String code = randomString();
        String invalidValue = "üks";
        String validValue = "123";

        Pages page = logInToAddPage()
                .addPhoneWithValue(invalidValue)
                .insert(customer());

        assertTrue(page.hasMessageBlock());

        SearchPage searchPage = logInToAddPage()
                .addPhoneWithValue(validValue)
                .insert(customer().withCode(code));

        ViewPage viewPage = searchPage.view(code);

        assertThat(viewPage.getPhoneValues(), contains(validValue));
    }

    @Test
    public void wrongPasswordFails_3() {
        Pages page = logInWith(USER, "wrong password");

        assertTrue(page.isLoginPage());
        assertTrue(page.hasMessageBlock());
    }

    @Test
    public void adminSeesAllTheLinks_3() {
        SearchPage searchPage = logInWith(ADMIN, ADMIN);

        assertThat(searchPage.menu().getIds(), contains(
                "menu_Search", "menu_Add", "menu_ClearData", "menu_logOut"));
    }

    @Test
    public void userSeesLessLinks_1() {
        SearchPage searchPage = logInWith(USER, USER);

        assertThat(searchPage.menu().getIds(), contains(
                "menu_Search", "menu_logOut"));
    }

    private AddPage logInToAddPage() {
        return logInToSearchPage().menu().addPage();
    }

    private SearchPage logInToSearchPage() {
        return logInWith(ADMIN, ADMIN);
    }
}
